package it.unibo.oop.lab.collections1;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

	private static final int START = 1000;
	private static final int STOP = 2000;
	private static final int ELEMS = 100000;
	private static final int READ = 1000;
	
	private static final long AFRICA_POPULATION = 1110635000L;
    private static final long AMERICAS_POPULATION = 972005000L;
    private static final long ANTARTICA_POPULATION = 0L;
    private static final long ASIA_POPULATION = 4298723000L;
    private static final long EUROPE_POPULATION = 742452000L;
    private static final long OCEANIA_POPULATION = 38304000L;
	
    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
    	/*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	final List<Integer> arrayList = new ArrayList<>();
    	for (int i = START; i < STOP; i++) {
    		arrayList.add(i);
    	}
    	System.out.println(arrayList);
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	final List<Integer> linkedList = new LinkedList<>(arrayList);
    	System.out.println(linkedList);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	final int temp = arrayList.get(arrayList.size() - 1);
    	arrayList.set((arrayList.size() - 1), arrayList.get(0));
		arrayList.set(0, temp);
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
		for (int elem : arrayList) {
			System.out.println(elem);
		}
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
		long time = System.nanoTime();
		for (int i = 0; i < ELEMS; i++) {
            arrayList.add(0, i);
        }
		time = System.nanoTime() - time;
		System.out.println("Inserting  " + ELEMS + " in the head of the array list took " + time  + " ns.");
		time = System.nanoTime();
		for (int i = 0; i < ELEMS; i++) {
            linkedList.add(0, i);
        }
		time = System.nanoTime() - time;
		System.out.println("Inserting  " + ELEMS + " in the head of the linked list took " + time  + " ns.");
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
		time = System.nanoTime();
		for (int i = 0; i < READ; i++) {
            arrayList.get(arrayList.size() / 2);
        }
		time = System.nanoTime() - time;
		System.out.println("Reading " + READ + " times the element positioned in the middle of the array list took " + time  + " ns.");
		time = System.nanoTime();
		for (int i = 0; i < READ; i++) {
            linkedList.get(arrayList.size() / 2);
        }
		time = System.nanoTime() - time;
		System.out.println("Reading " + READ + " times the element positioned in the middle of the linked list took " + time  + " ns.");
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
		final Map<String, Long> world = new HashMap<>();
        world.put("Africa", AFRICA_POPULATION);
        world.put("Americas", AMERICAS_POPULATION);
        world.put("Antarctica", ANTARTICA_POPULATION);
        world.put("Asia", ASIA_POPULATION);
        world.put("Europe", EUROPE_POPULATION);
        world.put("Oceania", OCEANIA_POPULATION);
        /*
         * 8) Compute the population of the world
         */
        long totPop = 0;
        for (final long population : world.values()) {
            totPop += population;
        }
        System.out.println("There are ~" + totPop + " people in ZA WARUDO.");
    }
}
